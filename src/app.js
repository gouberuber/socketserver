const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

io.on("connection", (socket) => {
  console.log("A client has connected!");

  socket.on("trade", (msg) => {
    console.log(msg);
  })
  socket.on("order", (msg) =>  {
    console.log(msg);
  })

});
io.on("disconnect", socket => {
  console.log("A client has disconnected!");
  
});




var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;
var ip_address = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

http.listen(port, ip_address, function(){
  console.log( "Listening on " + ip_address + ", port " + port );
});
